export const categories = [
    {
        id: 1,
        img: 'https://images.unsplash.com/photo-1556898016-f29272dea794?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8dCUyMHNoaXJ0fGVufDB8MnwwfHw%3D&auto=format&fit=crop&w=900&q=60',
        title: 'Shirts',
        cat: 'shirts',
    },
    {
        id: 2,
        img: 'https://images.unsplash.com/photo-1626307416562-ee839676f5fc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8amFja2V0c3xlbnwwfDJ8MHx8&auto=format&fit=crop&w=900&q=60',
        title: 'Jackets',
        cat: 'jackets',
    },
    {
        id: 3,
        img: 'https://images.unsplash.com/photo-1511284800349-cb63bc2e6c0e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjJ8fHNob3J0cyUyMGNsb3RoZXN8ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=900&q=60',
        title: 'Shorts',
        cat: 'shorts',
    },
];
