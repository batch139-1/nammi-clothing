import styled from 'styled-components';

export const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(77, 77, 77, 0.8)),
        url('https://images.unsplash.com/photo-1621959245005-1157d0c7508e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80');
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    display: flex;
    justify-content: center;
    align-items: center;
`;
export const Wrapper = styled.div`
    width: min(70vh, 400px);
    padding: 20px;
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    border-radius: 10px;
`;

export const Title = styled.h1`
    font-size: 2rem;
    font-weight: 500;
`;

export const Form = styled.form`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-items: center;
`;

export const Input = styled.input`
    width: min(40vh, 300px);
    margin: 20px 10px 0 0;
    padding: 3px;
`;

export const Agreement = styled.p`
    width: min(40vh, 300px);
    font-size: 0.75rem;
    margin: 5px 0;
`;

export const Button = styled.button`
    border: none;
    padding: 15px 20px;
    margin-top: 10px;
    cursor: pointer;
`;
