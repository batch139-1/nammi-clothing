import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 20px;
    display: flex;
    justify-content: space-evenly;
    border-bottom: 1px solid lightgray;
`;

export const Header = styled.div`
    padding: 20px;
    display: flex;
    justify-content: space-evenly;
    border-bottom: 1px solid lightgray;
`;

export const FirstName = styled.p`
    flex: 1;
`;
export const LastName = styled.p`
    flex: 1;
`;
export const Username = styled.p`
    flex: 1;
`;
export const Email = styled.p`
    flex: 1;
`;
export const Admin = styled.p`
    flex: 0.5;
    text-align: center;
`;

export const Action = styled.p`
    flex: 0.5;
    text-align: center;
`;

export const Button = styled.button`
    flex: 0.5;
    text-align: center;
`;
