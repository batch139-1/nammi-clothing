import { mobile } from '../responsive';
import styled from 'styled-components';

export const Container = styled.div``;

export const Wrapper = styled.div`
    padding: 20px;
`;

export const Title = styled.h1`
    font-weight: 300;
    text-align: center;
    ${mobile({ fontSize: '25px' })}
`;

export const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px;
    ${mobile({ padding: '0px', margin: 10 })}
`;

export const TopButton = styled.button`
    padding: 10px;
    cursor: pointer;
    border: ${(props) => props.type === 'filled' && 'none'};
    background-color: ${(props) =>
        props.type === 'filled' ? 'black' : 'transparent'};
    color: ${(props) => props.type === 'filled' && 'white'};
    ${mobile({ padding: '5px' })}
`;

export const TopTexts = styled.div`
    display: flex;
    ${mobile({ fontSize: '12px' })}
`;

export const TopText = styled.p`
    text-decoration: underline;
    cursor: pointer;
    margin: 0 10px;
`;

export const Bottom = styled.div`
    display: flex;
    justify-content: space-between;
    ${mobile({ display: 'block' })}
`;
export const Info = styled.div`
    flex: 3;
`;

export const Product = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const ProductDetails = styled.div`
    flex: 2;
    display: flex;
`;

export const Image = styled.img`
    width: 150px;
    ${mobile({ height: '100px', width: '100px' })}
`;

export const Details = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    ${mobile({ padding: '10px' })}
`;

export const ProductName = styled.p`
    ${mobile({ fontSize: '10px' })}
`;

export const ProductId = styled.span`
    ${mobile({ fontSize: '10px' })}
`;

export const ProductColor = styled.div`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${(props) => props.color};
`;

export const ProductSize = styled.span`
    ${mobile({ fontSize: '10px' })}
`;

export const PriceDetail = styled.span`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    ${mobile({ fontSize: '10px', display: 'block' })}
`;

export const ProductAmountContainer = styled.div`
    display: flex;
    align-items: center;
`;

export const ProductAmount = styled.div`
    font-size: 1.15rem;
    margin: 5px;
    ${mobile({ fontSize: '10px' })}
`;

export const ProductPrice = styled.div`
    font-style: 30px;
    margin: 5px;
`;

export const Hr = styled.hr`
    background-color: #ebebeb;
    border: none;
    height: 2px;
    margin: 5px 0;
`;

export const Summary = styled.div`
    flex: 1;
    border: 0.5px solid lightgray;
    border-radius: 10px;
    padding: 20px;
    height: 50vh;
    ${mobile({ height: '180px' })}
`;

export const SummaryTitle = styled.h1`
    font-weight: 200;
    ${mobile({ fontSize: '16px' })}
`;

export const SummaryItem = styled.div`
    margin: 30px 0;
    display: flex;
    justify-content: space-between;
    font-weight: ${(props) => props.type === 'total' && '500'};
    font-size: ${(props) => props.type === 'total' && '1.1rem'};
    ${mobile({ margin: '10px 0' })}
    ${mobile({ fontSize: '12px' })}
`;

export const SummaryItemText = styled.span``;

export const SummaryItemPrice = styled.span``;

export const Button = styled.button`
    width: 100%;
    padding: 10px;
    background-color: black;
    color: white;
    font-weight: 600;
    cursor: pointer;
`;
