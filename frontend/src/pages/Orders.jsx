import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { useLocation, Link } from 'react-router-dom';
import axios from 'axios';
// prettier-ignore
import { Container, Wrapper, Title, Top, TopButton, TopTexts, TopText, Bottom, Info, Product, ProductDetails, ProductId, Image, Details, ProductName, ProductColor, ProductSize, PriceDetail, ProductPrice, Hr, OrderContainer, Button } from './orders-styling'

const Order = () => {
    let token = localStorage.getItem('token');
    let userId = localStorage.getItem('userId');
    let location = useLocation();
    const id = location.pathname.split('/')[2];
    const [orders, setOrders] = useState({});
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getOrder = async () => {
            try {
                const res = await axios.get(
                    `https://nammi-clothing-api.herokuapp.com/api/order/${id}`,
                    {
                        headers: {
                            authorization: `Bearer ${token}`,
                        },
                    }
                );
                setOrders(res.data);
                setLoading(false);
            } catch {}
        };
        getOrder();
    }, [id, token]);

    const orderHandler = async (order) => {
        const { isDelivered, _id } = order;
        if (!isDelivered) {
            try {
                axios.put(
                    `https://nammi-clothing-api.herokuapp.com/api/order/${_id}/update`,
                    { isDelivered: true },
                    {
                        headers: {
                            authorization: `Bearer ${token}`,
                        },
                    }
                );
                alert(`Order Received!`);
                window.location.reload();
            } catch (error) {}
        } else {
            try {
                axios.delete(
                    `https://nammi-clothing-api.herokuapp.com/api/order/${_id}/delete`,
                    {
                        headers: {
                            authorization: `Bearer ${token}`,
                        },
                    }
                );
                alert(`Order Deleted`);
                window.location.reload();
            } catch (error) {
                console.log(error);
            }
        }
    };

    console.log(orders);

    if (loading) {
        return `loading...`;
    } else {
        return (
            <Container>
                <Navbar />
                <Wrapper>
                    <Title>Your Orders</Title>
                    <Top>
                        <TopButton>
                            <Link
                                to="/products"
                                style={{ textDecoration: 'none' }}
                            >
                                Continue Shopping
                            </Link>
                        </TopButton>
                        <TopTexts>
                            <Link to={`/cart/${userId}`}>
                                <TopText>Shopping Bag</TopText>
                            </Link>
                            <Link to={`/order/${userId}`}>
                                <TopText>Your Orders</TopText>
                            </Link>
                        </TopTexts>
                        <TopButton>
                            <Link to="/" style={{ textDecoration: 'none' }}>
                                Back to Home
                            </Link>
                        </TopButton>
                    </Top>
                    <Bottom>
                        <Info>
                            {orders.length === 0 && (
                                <Title>You have no orders yet.</Title>
                            )}
                            {orders?.map((order) => (
                                <OrderContainer key={order._id}>
                                    {order?.products?.map((product) => (
                                        <Product key={product._id}>
                                            <ProductDetails>
                                                <Image src={product?.img} />
                                                <Details>
                                                    <ProductName>
                                                        <b>Product:</b>{' '}
                                                        {product?.title}
                                                    </ProductName>
                                                    <ProductId>
                                                        <b>ID:</b>{' '}
                                                        {product?._id}
                                                    </ProductId>
                                                    <ProductColor
                                                        color={product.color}
                                                    />
                                                    <ProductSize>
                                                        <b>Size:</b>{' '}
                                                        {product?.size}
                                                    </ProductSize>
                                                    <ProductSize>
                                                        <b>Status:</b>{' '}
                                                        {order?.isDelivered
                                                            ? `Delivered`
                                                            : `Pending`}
                                                    </ProductSize>
                                                </Details>
                                            </ProductDetails>
                                            <PriceDetail>
                                                <ProductPrice>
                                                    Php {product?.price}
                                                </ProductPrice>
                                                {order?.isDelivered ? (
                                                    <Button
                                                        onClick={() =>
                                                            orderHandler(order)
                                                        }
                                                    >
                                                        Delete Order
                                                    </Button>
                                                ) : (
                                                    <Button
                                                        onClick={() =>
                                                            orderHandler(order)
                                                        }
                                                    >
                                                        Order Received
                                                    </Button>
                                                )}
                                            </PriceDetail>
                                        </Product>
                                    ))}
                                </OrderContainer>
                            ))}
                            <Hr />
                        </Info>
                    </Bottom>
                </Wrapper>
                <Footer />
            </Container>
        );
    }
};

export default Order;
