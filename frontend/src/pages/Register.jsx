import axios from 'axios';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../components/Navbar';
// prettier-ignore
import { Container, Wrapper, Title, Form, Input, Agreement, Button } from './register-styling'

const Register = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    });

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (
            !state.firstName &&
            !state.lastName &&
            !state.username &&
            !state.email &&
            !state.password &&
            !state.confirmPassword
        ) {
            alert(`Please completely input your details.`);
        } else if (state.password !== state.confirmPassword) {
            alert(`Password did not match.`);
        } else
            axios
                .post(
                    'https://nammi-clothing-api.herokuapp.com/api/auth/register',
                    state
                )
                .then((data) => {
                    if (data.status === 201) {
                        window.location.href = 'http://localhost:3000/login';
                    }
                });
    };
    return (
        <div>
            <Navbar />
            <Container>
                <Wrapper>
                    <Title>Register</Title>
                    <Form>
                        <Input
                            type="string"
                            placeholder="First Name"
                            id="firstName"
                            onInput={handleChange}
                            value={state.firstName}
                        />
                        <Input
                            type="string"
                            placeholder="Last Name"
                            id="lastName"
                            onChange={handleChange}
                            value={state.lastName}
                        />
                        <Input
                            type="string"
                            placeholder="Username"
                            id="username"
                            onChange={handleChange}
                            value={state.username}
                        />
                        <Input
                            type="email"
                            placeholder="Email"
                            id="email"
                            onChange={handleChange}
                            value={state.email}
                        />
                        <Input
                            type="password"
                            placeholder="Password"
                            id="password"
                            onChange={handleChange}
                            value={state.password}
                        />
                        <Input
                            type="password"
                            placeholder="Confirm Password"
                            id="confirmPassword"
                            onChange={handleChange}
                            value={state.confirmPassword}
                        />
                        <Agreement>
                            By creating an account, I consent to the processing
                            of my personal data in accordance with the{' '}
                            <b>PRIVACY POLICY</b>
                        </Agreement>
                        <Agreement>
                            Already have an account?{' '}
                            <Link to="/login">Login</Link>
                        </Agreement>
                        <Agreement>
                            Back to{' '}
                            <Link to="/" style={{ textDecoration: 'none' }}>
                                homepage
                            </Link>
                        </Agreement>
                        <Button onClick={handleSubmit}>Create Account</Button>
                    </Form>
                </Wrapper>
            </Container>
        </div>
    );
};

export default Register;
