import styled from 'styled-components';
import { mobile } from '../responsive';

export const Container = styled.div``;
export const Wrapper = styled.div`
    padding: 50px;
    display: flex;
    ${mobile({ display: 'block' })}
`;
export const ImgContainer = styled.div`
    flex: 1;
`;

export const Image = styled.img`
    width: 100%;
`;

export const InfoContainer = styled.div`
    flex: 1;
    padding: 0 50px;
    ${mobile({ padding: '0' })}
`;

export const Title = styled.h1`
    font-weight: 200;
    ${mobile({ fontSize: '20px' })}
`;

export const Desc = styled.p`
    margin: 20px 0;
`;

export const Price = styled.span`
    font-weight: 200;
    font-size: 2rem;
    ${mobile({ fontSize: '1.5rem' })}
`;

export const FilterContainer = styled.div`
    width: 50%;
    margin: 30px 0;
    display: flex;
    justify-content: space-between;
`;

export const Filter = styled.div`
    display: flex;
    align-items: center;
`;

export const FilterTitle = styled.span`
    font-style: 20px;
    font-weight: 300;
`;

export const FilterColor = styled.div`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${(props) => props.color};
    margin: 0 5px;
    cursor: pointer;
    border: 2px solid black;
`;

export const FilterSize = styled.select`
    margin-left: 10px;
    padding: 5px;
`;

export const FilterSizeOption = styled.option``;

export const AddContainer = styled.div`
    display: flex;
    align-items: center;
    width: 50%;
    justify-content: space-between;
`;

export const AmountContainer = styled.div`
    display: flex;
    align-items: center;
    font-weight: 500;
`;

export const Amount = styled.span`
    width: 30px;
    height: 30px;
    border-radius: 10px;
    border: 1px solid teal;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 5px;
`;

export const Button = styled.button`
    padding: 15px;
    border: 2px solid red;
    background-color: white;
    cursor: pointer;
    font-weight: 500;
    transition: all 0.1s ease-in-out;
    ${mobile({ padding: '10px', marginLeft: '30px' })};

    &:hover {
        background-color: #eeeeee;
    }
`;
