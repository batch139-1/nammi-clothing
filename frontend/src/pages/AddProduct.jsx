import React, { useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import axios from 'axios';
// prettier-ignore
import { Container, Wrapper, Title, Form, Input, Button } from './addproduct-styling';

const AddProduct = () => {
    let token = localStorage.getItem('token');
    let categories;
    let sizes;
    let colors;
    const [state, setState] = useState({
        title: '',
        desc: '',
        img: '',
        categories: [],
        size: [],
        color: [],
        price: '',
    });

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    const toArray = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value.split(' '),
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        axios
            .post(
                'https://nammi-clothing-api.herokuapp.com/api/products/create',
                state,
                {
                    headers: {
                        authorization: `Bearer ${token}`,
                    },
                }
            )
            .then((data) => {
                if (data.status === 200) {
                    alert(`Product Added`);
                }
            });
    };
    return (
        <div>
            <Navbar></Navbar>
            <Container>
                <Wrapper>
                    <Title>Add Product</Title>
                    <Form>
                        <Input
                            type="string"
                            placeholder="Title"
                            id="title"
                            onInput={handleChange}
                            value={state.title}
                        />
                        <Input
                            type="string"
                            placeholder="Description"
                            id="desc"
                            onChange={handleChange}
                            value={state.desc}
                        />
                        <Input
                            type="string"
                            placeholder="Image Link"
                            id="img"
                            onChange={handleChange}
                            value={state.img}
                        />
                        <Input
                            type="string"
                            placeholder="Categories"
                            id="categories"
                            onChange={toArray}
                            value={categories}
                        />
                        <Input
                            type="string"
                            placeholder="Sizes"
                            id="size"
                            onChange={toArray}
                            value={sizes}
                        />
                        <Input
                            type="string"
                            placeholder="Colors"
                            id="color"
                            onChange={toArray}
                            value={colors}
                        />
                        <Input
                            type="number"
                            placeholder="Price"
                            id="price"
                            onChange={handleChange}
                            value={state.price}
                        />
                        <Button onClick={handleSubmit}>Add Product</Button>
                    </Form>
                </Wrapper>
            </Container>
            <Footer></Footer>
        </div>
    );
};

export default AddProduct;
