import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { Add, Remove } from '@material-ui/icons';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import UpdateProduct from '../components/UpdateProduct';
// prettier-ignore
import { Container, Wrapper, ImgContainer, Image, InfoContainer, Title, Desc, Price, FilterContainer, Filter, FilterTitle, FilterColor, FilterSize, FilterSizeOption, AddContainer, AmountContainer, Amount, Button } from './product-styling'

const Product = () => {
    let location = useLocation();
    let token = localStorage.getItem('token');
    let userId = localStorage.getItem('userId');
    let admin = localStorage.getItem('isAdmin');
    const id = location.pathname.split('/')[2];
    const [product, setProduct] = useState({});
    const [amount, setAmount] = useState(1);
    const [color, setColor] = useState('');
    const [size, setSize] = useState('');

    const addItem = {
        userId: userId,
        productId: id,
        quantity: amount,
        size: size,
        color: color,
    };

    useEffect(() => {
        const getProduct = async () => {
            try {
                const res = await axios.get(
                    `https://nammi-clothing-api.herokuapp.com/api/products/find/${id}`
                );
                setProduct(res.data);
            } catch {}
        };
        getProduct();
    }, [id]);

    const addToCartHandler = async () => {
        if (!token) {
            alert(`Login first to continue.`);
            window.location.href = 'http://localhost:3000/login';
        } else if (!addItem.color || !addItem.size) {
            alert(`Please complete product details before adding to cart`);
        } else {
            await axios.post(
                `https://nammi-clothing-api.herokuapp.com/api/cart/add/${userId}`,
                addItem,
                {
                    headers: {
                        authorization: `Bearer ${token}`,
                    },
                }
            );
            alert(`Product Added`);
            window.location.reload();
        }
    };

    return (
        <Container>
            <Navbar />
            <Wrapper>
                <ImgContainer>
                    <Image src={product.img} />
                </ImgContainer>
                <InfoContainer>
                    <Title>{product.title}</Title>
                    <Desc>{product.desc}</Desc>
                    <Price>{product.price}</Price>
                    <FilterContainer>
                        <Filter>
                            <FilterTitle>Color: </FilterTitle>
                            {product.color?.map((c) => (
                                <FilterColor
                                    color={c}
                                    key={c}
                                    onClick={() => setColor(c)}
                                />
                            ))}
                        </Filter>
                        <Filter>
                            <FilterTitle>Size: </FilterTitle>
                            <FilterSize
                                onChange={(e) => setSize(e.target.value)}
                            >
                                {product.size?.map((s) => (
                                    <FilterSizeOption key={s}>
                                        {s}
                                    </FilterSizeOption>
                                ))}
                            </FilterSize>
                        </Filter>
                    </FilterContainer>

                    <AddContainer>
                        <AmountContainer>
                            <Remove
                                onClick={() => {
                                    if (amount > 1) {
                                        setAmount(amount - 1);
                                    }
                                }}
                            />
                            <Amount>{amount}</Amount>
                            <Add onClick={() => setAmount(amount + 1)} />
                        </AmountContainer>
                        <Button onClick={addToCartHandler}>ADD TO CART</Button>
                    </AddContainer>
                    {JSON.parse(admin) && <UpdateProduct />}
                </InfoContainer>
            </Wrapper>
            <Footer />
        </Container>
    );
};

export default Product;
