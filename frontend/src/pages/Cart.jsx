import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { Add, Remove } from '@material-ui/icons';
import { useLocation, Link } from 'react-router-dom';
import axios from 'axios';
// prettier-ignore
import { Container, Wrapper, Title, Top, TopButton, TopTexts, TopText, Bottom, Info, Product, ProductDetails, Image, Details, ProductName, ProductId, ProductColor, ProductSize, PriceDetail, ProductAmountContainer, ProductAmount, ProductPrice, Hr, Summary, SummaryTitle, SummaryItem, SummaryItemText, SummaryItemPrice, Button } from './cart-styling';

const Cart = () => {
    let token = localStorage.getItem('token');
    let userId = localStorage.getItem('userId');
    let location = useLocation();
    const id = location.pathname.split('/')[2];
    const [cart, setCart] = useState({});

    useEffect(() => {
        const getCart = async () => {
            if (token) {
                try {
                    const res = await axios.get(
                        `https://nammi-clothing-api.herokuapp.com/api/cart/${id}`,
                        {
                            headers: {
                                authorization: `Bearer ${token}`,
                            },
                        }
                    );
                    setCart(res.data);
                } catch (error) {}
            }
        };
        getCart();
        return () => {};
    }, [id, token]);

    const handleCheckOut = async (e) => {
        const checkOut = await axios.post(
            `https://nammi-clothing-api.herokuapp.com/api/order/${userId}/checkout`,
            {},
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            }
        );
        if (checkOut) {
            alert(`Items Successfully Ordered`);
            window.location.href = `http://localhost:3000/order/${userId}`;
        }
    };

    return (
        <Container>
            <Navbar />
            <Wrapper>
                <Title>Your Cart</Title>
                <Top>
                    <TopButton>
                        <Link to="/products" style={{ textDecoration: 'none' }}>
                            Continue Shopping
                        </Link>
                    </TopButton>
                    <TopTexts>
                        <Link to={`/cart/${userId}`}>
                            <TopText>Shopping Bag</TopText>
                        </Link>
                        <Link to={`/order/${userId}`}>
                            <TopText>Your Orders</TopText>
                        </Link>
                    </TopTexts>
                    <TopButton>
                        <Link to="/" style={{ textDecoration: 'none' }}>
                            Back to Home
                        </Link>
                    </TopButton>
                </Top>
                <Bottom>
                    <Info>
                        {cart === null && <Title>Your cart is empty.</Title>}
                        {cart?.products?.map((product) => (
                            <Product key={product._id}>
                                <ProductDetails>
                                    <Image src={product.img} />
                                    <Details>
                                        <ProductName>
                                            <b>Product:</b> {product.title}
                                        </ProductName>
                                        <ProductId>
                                            <b>ID:</b> {product._id}
                                        </ProductId>
                                        <ProductColor color={product.color} />
                                        <ProductSize>
                                            <b>Size:</b> {product.size}
                                        </ProductSize>
                                    </Details>
                                </ProductDetails>
                                <PriceDetail>
                                    <ProductAmountContainer>
                                        <Remove />
                                        <ProductAmount>
                                            {product.quantity}
                                        </ProductAmount>
                                        <Add />
                                    </ProductAmountContainer>
                                    <ProductPrice>
                                        Php {product.price}
                                    </ProductPrice>
                                </PriceDetail>
                            </Product>
                        ))}
                        <Hr />
                    </Info>
                    <Summary>
                        <SummaryTitle>Summary</SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal</SummaryItemText>
                            <SummaryItemPrice>
                                Php {cart?.totalAmount || 0}
                            </SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>
                                Estimated Shipping
                            </SummaryItemText>
                            <SummaryItemPrice>Php 45</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Shipping Discount</SummaryItemText>
                            <SummaryItemPrice>Php 40</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem type="total">
                            <SummaryItemText>Total</SummaryItemText>
                            <SummaryItemPrice>
                                Php {cart?.totalAmount - 5 || 0}
                            </SummaryItemPrice>
                        </SummaryItem>
                        <Button onClick={handleCheckOut}>Checkout Now</Button>
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer />
        </Container>
    );
};

export default Cart;
