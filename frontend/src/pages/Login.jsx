import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Navbar from '../components/Navbar';
// prettier-ignore
import {Container, Wrapper, Title, Form, Input, Links, Button } from './login-styling'

const Login = () => {
    const [state, setState] = useState({
        username: '',
        password: '',
    });

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!state.username && !state.password) {
            alert(`Please input credentials`);
        } else
            axios
                .post(
                    'https://nammi-clothing-api.herokuapp.com/api/auth/login',
                    state
                )
                .then((res) => {
                    const { _id, isAdmin, token } = res.data;
                    localStorage.setItem('isAdmin', isAdmin);
                    localStorage.setItem('userId', _id);
                    localStorage.setItem('token', token);
                    window.location.href = 'http://localhost:3000/';
                })
                .catch((error) => {
                    alert(`Invalid username or password`);
                });
    };

    return (
        <div>
            <Navbar />
            <Container>
                <Wrapper>
                    <Title>Login</Title>
                    <Form>
                        <Input
                            type="string"
                            placeholder="Username"
                            id="username"
                            onChange={handleChange}
                            value={state.username}
                        />
                        <Input
                            type="password"
                            placeholder="Password"
                            id="password"
                            onChange={handleChange}
                            value={state.password}
                        />
                        <Button onClick={handleSubmit}>Login</Button>
                        <Links>
                            <Link to="#" style={{ textDecoration: 'none' }}>
                                Forgot password?
                            </Link>
                        </Links>
                        <Links>
                            <Link
                                to="/register"
                                style={{ textDecoration: 'none' }}
                            >
                                Create an account
                            </Link>
                        </Links>
                        <Links>
                            <Link to="/" style={{ textDecoration: 'none' }}>
                                Back to homepage
                            </Link>
                        </Links>
                    </Form>
                </Wrapper>
            </Container>
        </div>
    );
};

export default Login;
