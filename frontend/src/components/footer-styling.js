import styled from 'styled-components';
import { mobile } from '../responsive';

export const Container = styled.div`
    display: flex;
    background-color: #ffede7;
    ${mobile({ display: 'block' })}
`;

export const Left = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 20px;
`;

export const Desc = styled.p`
    margin: 20px 0;
    ${mobile({ fontSize: '12px', margin: '10px 0' })}
`;

export const SocialContainer = styled.div`
    display: flex;
`;

export const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background-color: #${(props) => props.color};
    display: flex;
    justify-content: center;
    align-items: center;
    margin-right: 30px;
    color: white;
    ${mobile({ width: '35px', height: '35px', marginRight: '20px' })}
`;

export const Center = styled.div`
    flex: 1;
    padding: 20px;
`;

export const Title = styled.h3`
    margin-bottom: 30px;
    ${mobile({ marginBottom: '20px' })}
    ${mobile({ fontSize: '16px' })}
`;

export const List = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    flex-wrap: wrap;
`;

export const ListItem = styled.li`
    width: 50%;
    margin-bottom: 10px;
    ${mobile({ fontSize: '12px' })}
`;

export const Right = styled.div`
    flex: 1;
    padding: 20px;
`;

export const ContactItem = styled.p`
    margin-bottom: 7px;
    ${mobile({ fontSize: '13px' })}
`;

export const Payment = styled.img``;

export const Logo = styled.h1`
    ${mobile({ fontSize: '20px' })}
`;
