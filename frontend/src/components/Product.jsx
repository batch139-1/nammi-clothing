import React from 'react';
import { RemoveRedEye } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { Info, Container, Image, Icon } from './product-styling';

const Product = ({ item }) => {
    const { img } = item;
    return (
        <Container>
            <Image src={img} />
            <Info>
                <Icon>
                    <Link to={`/product/${item._id}`}>
                        <RemoveRedEye />
                    </Link>
                </Icon>
            </Info>
        </Container>
    );
};

export default Product;
