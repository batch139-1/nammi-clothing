import styled from 'styled-components';
import { mobile } from '../responsive';

export const Info = styled.div`
    opacity: 0;
    background-color: rgba(0, 0, 0, 0.2);
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    display: flex;
    justify-content: center;
    flex-direction: column;
    transition: all 0.2s ease;
    ${mobile({ opacity: '1' })}
`;

export const Container = styled.div`
    flex: 1;
    margin: 3px;
    height: 40vh;
    position: relative;

    &:hover ${Info} {
        opacity: 1;
    }
`;

export const Image = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
`;

export const Title = styled.h1`
    font-weight: 500;
    text-align: center;
    color: white;
    ${mobile({ fontSize: '20px' })}
`;
export const Button = styled.button`
    width: 25%;
    margin: 10px auto;
    border: none;
    padding: 5px;
    cursor: pointer;
    background-color: white;
    color: gray;
    font-weight: 500;
    ${mobile({ width: '50px' })}
`;
