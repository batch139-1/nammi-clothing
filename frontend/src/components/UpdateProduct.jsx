import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
// prettier-ignore
import { Container, Wrapper, Title, Form, Input, Button, ArchiveButton, UnarchiveButton } from './updateproduct-styling';

const UpdateProduct = () => {
    let location = useLocation();
    const id = location.pathname.split('/')[2];
    let token = localStorage.getItem('token');
    let categories;
    let sizes;
    let colors;
    let title;
    let desc;
    let img;
    let price;
    const [state, setState] = useState({});
    const [product, setProduct] = useState({});

    const handleChange = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    const toArray = (e) => {
        const { id, value } = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value.split(' '),
        }));
    };

    useEffect(() => {
        const getProduct = async () => {
            try {
                const res = await axios.get(
                    `https://nammi-clothing-api.herokuapp.com/api/products/find/${id}`
                );
                setProduct(res.data);
            } catch {}
        };
        getProduct();
    }, [id]);

    const handleSubmit = async (e) => {
        try {
            axios.put(
                `https://nammi-clothing-api.herokuapp.com/api/products/${id}/update`,
                state,
                {
                    headers: {
                        authorization: `Bearer ${token}`,
                    },
                }
            );
            alert(`Product Edited`);
            window.location.reload();
        } catch (error) {}
    };

    const handleArchive = async (e) => {
        if (inStock) {
            try {
                axios.put(
                    `https://nammi-clothing-api.herokuapp.com/api/products/${id}/update`,
                    { inStock: false },
                    {
                        headers: {
                            authorization: `Bearer ${token}`,
                        },
                    }
                );
                alert(`Product Archived`);
                window.location.reload();
            } catch (error) {}
        } else {
            try {
                axios.put(
                    `https://nammi-clothing-api.herokuapp.com/api/products/${id}/update`,
                    { inStock: true },
                    {
                        headers: {
                            authorization: `Bearer ${token}`,
                        },
                    }
                );
                alert(`Product Unarchived`);
                window.location.reload();
            } catch (error) {}
        }
    };

    const inStock = product?.inStock;

    return (
        <div>
            <Container>
                <Wrapper>
                    {inStock && (
                        <ArchiveButton onClick={handleArchive}>
                            Archive Product
                        </ArchiveButton>
                    )}
                    {!inStock && (
                        <UnarchiveButton onClick={handleArchive}>
                            Unarchive Product
                        </UnarchiveButton>
                    )}
                    <Title>Edit Product</Title>
                    <Form>
                        <Input
                            type="string"
                            placeholder="Title"
                            id="title"
                            onInput={handleChange}
                            value={title}
                        />
                        <Input
                            type="string"
                            placeholder="Description"
                            id="desc"
                            onChange={handleChange}
                            value={desc}
                        />
                        <Input
                            type="string"
                            placeholder="Image Link"
                            id="img"
                            onChange={handleChange}
                            value={img}
                        />
                        <Input
                            type="string"
                            placeholder="Categories"
                            id="categories"
                            onChange={toArray}
                            value={categories}
                        />
                        <Input
                            type="string"
                            placeholder="Sizes"
                            id="size"
                            onChange={toArray}
                            value={sizes}
                        />
                        <Input
                            type="string"
                            placeholder="Colors"
                            id="color"
                            onChange={toArray}
                            value={colors}
                        />
                        <Input
                            type="number"
                            placeholder="Price"
                            id="price"
                            onChange={handleChange}
                            value={price}
                        />
                        <Button onClick={handleSubmit}>Edit Product</Button>
                    </Form>
                </Wrapper>
            </Container>
        </div>
    );
};

export default UpdateProduct;
