import React from 'react';
// prettier-ignore
import { Section, Container, Title, Description, Button, Line, Link } from './jumbotron-styling';

const Jumbotron = () => {
    return (
        <Section>
            <Container>
                <Title>Welcome to Nammi Clothing</Title>
                <Description>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Dignissimos facilis dolorem eligendi distinctio neque libero
                    molestias animi. Aliquam atque obcaecati laboriosam expedita
                    natus impedit maxime.
                </Description>
                <Line />
                <Link href="/products" role="button">
                    <Button>Shop Now!</Button>
                </Link>
            </Container>
        </Section>
    );
};

export default Jumbotron;
