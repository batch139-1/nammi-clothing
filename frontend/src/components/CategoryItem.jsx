import React from 'react';
import { Link } from 'react-router-dom';
import { Info, Container, Image, Title, Button } from './categoryitem-styling';

const CategoryItem = ({ item }) => {
    const { img, title } = item;
    return (
        <Container>
            <Link to={`/products/`}>
                <Image src={img} />
                <Info>
                    <Title>{title}</Title>
                    <Button>Shop Now</Button>
                </Info>
            </Link>
        </Container>
    );
};

export default CategoryItem;
