import styled from 'styled-components';
import { mobile } from '../responsive';

export const Section = styled.div`
    width: 100%;
    height: 60vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: linear-gradient(
            45deg,
            rgba(255, 255, 255, 0.8),
            rgba(255, 255, 255, 0.5)
        ),
        url('https://images.unsplash.com/photo-1489987707025-afc232f7ea0f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80');
    background-size: cover;
    background-repeat: no-repeat;
`;

export const Container = styled.div`
    padding: 20px;
`;

export const Title = styled.h1`
    font-size: 3rem;
    margin-bottom: 3rem;
    font-weight: 600;
    ${mobile({ fontSize: '2rem', marginBottom: '1rem' })}
`;

export const Description = styled.p`
    width: min(70vh, 1000px);
    color: #242424;
    margin-bottom: 1rem;
    ${mobile({ fontSize: '14px', width: '320px' })}
`;

export const Button = styled.button`
    padding: 10px 15px;
    margin-top: 1rem;
    font-weight: 500;
    font-size: 1.1rem;
    cursor: pointer;
    border: none;
    border-radius: 5px;
    ${mobile({ padding: '7px 12px', fontSize: '0.9rem' })}
`;

export const Line = styled.hr`
    width: 80%;
`;

export const Link = styled.a``;
