import React from 'react';
import CategoryItem from './CategoryItem';
import { categories } from '../data';
import { Container } from './categories-styling';

const Categories = () => {
    return (
        <Container>
            {categories?.map((item) => (
                <CategoryItem item={item} key={item.id} />
            ))}
        </Container>
    );
};

export default Categories;
