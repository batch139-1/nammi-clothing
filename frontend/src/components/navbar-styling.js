import styled from 'styled-components';
import { mobile } from '../responsive';

export const Nav = styled.div`
    width: 100%;
    height: 50px;
    overflow-x: hidden;
`;

export const Container = styled.div`
    display: flex;
    padding: 10px 20px;
`;

export const Left = styled.div`
    flex: 1;
    align-items: center;
`;

export const Logo = styled.h1`
    font-weight: 600;
    ${mobile({ fontSize: '20px' })}
`;

export const Center = styled.div`
    display: flex;
    flex: 1;
    justify-content: flex-end;
    align-items: center;
    ${mobile({ display: 'none' })}
`;

export const Right = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    ${mobile({ justifyContent: 'space-between', flex: 2.5 })}
`;

export const Items = styled.div`
    font-size: 14px;
    cursor: pointer;
    ${mobile({ fontSize: '11px' })}
`;
