import styled from 'styled-components';

export const Container = styled.div`
    margin-top: 20px;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    display: flex;
    align-items: center;
`;
export const Wrapper = styled.div`
    width: min(70vh, 400px);
    padding: 20px;
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    border-radius: 10px;
`;

export const Title = styled.h1`
    font-size: 2rem;
    font-weight: 500;
`;

export const Form = styled.form`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-items: center;
`;

export const Input = styled.input`
    width: min(40vh, 300px);
    margin: 20px 10px 0 0;
    padding: 3px;
`;

export const Button = styled.button`
    border: none;
    padding: 15px 20px;
    margin-top: 10px;
    cursor: pointer;
`;

export const ArchiveButton = styled.button`
    background-color: #ff4343;
    border: none;
    padding: 15px 20px;
    margin-top: 10px;
    cursor: pointer;
    margin-bottom: 20px;
`;

export const UnarchiveButton = styled.button`
    background-color: #00c400;
    border: none;
    padding: 15px 20px;
    margin-top: 10px;
    cursor: pointer;
    margin-bottom: 20px;
`;
