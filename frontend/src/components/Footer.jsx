import React from 'react';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import MapIcon from '@mui/icons-material/Map';
import PhoneEnabledIcon from '@mui/icons-material/PhoneEnabled';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { Link } from 'react-router-dom';
// prettier-ignore
import { Container, Left, Desc, SocialContainer, SocialIcon, Center, Title, List, ListItem, Right, ContactItem, Payment, Logo } from './footer-styling';

const Footer = () => {
    return (
        <Container>
            <Left>
                <Logo>NAMMI</Logo>
                <Desc>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Totam, qui. Tempora veniam ea expedita fugit dignissimos,
                    fugiat culpa qui sunt id placeat numquntium sint.
                </Desc>
                <SocialContainer>
                    <SocialIcon color="3b5998">
                        <FacebookIcon />
                    </SocialIcon>
                    <SocialIcon color="00acee">
                        <TwitterIcon />
                    </SocialIcon>
                    <SocialIcon color="E4405F">
                        <InstagramIcon />
                    </SocialIcon>
                </SocialContainer>
            </Left>

            <Center>
                <Title>Navigate</Title>
                <List>
                    <ListItem>
                        <Link to="/">Home</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="#">About Us</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/products">Shop</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/cart">Cart</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/register">Register</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/login">Login</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="#">Careers</Link>
                    </ListItem>
                    <ListItem>
                        <Link to="#">Terms</Link>
                    </ListItem>
                </List>
            </Center>
            <Right>
                <Title>Contact</Title>
                <ContactItem>
                    <MapIcon style={{ marginRight: '10px' }} />
                    0429 Malitlit, Los Baños 4030
                </ContactItem>
                <ContactItem>
                    <PhoneEnabledIcon style={{ marginRight: '10px' }} />
                    +63 243 4432
                </ContactItem>
                <ContactItem>
                    <MailOutlineIcon style={{ marginRight: '10px' }} />
                    contact@nammi.dev
                </ContactItem>
                <Payment src="https://i.ibb.co/Qfvn4z6/payment.png" />
            </Right>
        </Container>
    );
};

export default Footer;
