import { hashSync, compareSync } from 'bcrypt';
import jwt from 'jsonwebtoken';
import express from 'express';

import User from '../models/User.js';

const router = express.Router();

export const registerUser = async (req, res) => {
    const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        username: req.body.username,
        email: req.body.email,
        password: hashSync(req.body.password, 10),
    });
    try {
        const savedUser = await newUser.save();
        res.status(201).json(savedUser);
    } catch (error) {
        res.status(500).json(error);
    }
};

export const loginUser = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username });
        if (!user) {
            res.status(401).json('Invalid username or password');
        }
        const isPasswordCorrect = compareSync(req.body.password, user.password);
        if (!isPasswordCorrect) {
            res.status(401).json('Invalid username or password');
        } else {
            const token = jwt.sign(
                {
                    id: user.id,
                    isAdmin: user.isAdmin,
                },
                process.env.JWT_SECRET,
                { expiresIn: '5d' }
            );
            const { password, ...others } = user._doc;
            res.status(200).json({ ...others, token });
        }
    } catch (error) {
        res.status(500).json(error);
    }
};

export default router;
