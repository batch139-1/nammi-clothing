import express from 'express';
import Cart from '../models/Cart.js';
import Order from '../models/Order.js';
const router = express.Router;

export const checkOut = async (req, res) => {
    try {
        let cart = await Cart.findOne({ userId: req.params.id });
        if (cart) {
            const { userId, products, totalAmount } = cart;
            const newOrder = await Order.create({
                userId: userId,
                products: products,
                totalAmount: totalAmount,
            });
            await Cart.findByIdAndDelete({ _id: cart._id });
            res.status(200).json(newOrder);
        } else {
            return `no products in cart`;
        }
    } catch (error) {
        res.status(500).json(error);
    }
};

export const updateOrder = async (req, res) => {
    try {
        const updatedOrder = await Order.findByIdAndUpdate(
            req.params.id,
            { $set: req.body },
            { new: true }
        );
        res.status(200).json(updatedOrder);
    } catch (error) {
        res.status(500).json(error);
    }
};

export const deleteOrder = async (req, res) => {
    try {
        await Order.findByIdAndRemove(req.params.id);
        res.status(200).json('Order deleted');
    } catch (error) {
        res.status(500).json(error);
    }
};

export const getOrder = async (req, res) => {
    try {
        const orders = await Order.find({ userId: req.params.id });
        res.status(200).json(orders);
    } catch (error) {
        res.status(500).json(error);
    }
};

export const getAllOrders = async (req, res) => {
    try {
        const orders = await Order.find();
        res.status(200).json(orders);
    } catch (error) {
        res.status(500).json(error);
    }
};

export default router;
