import { verifyTokenAndAuth, verifyTokenAndAdmin } from './verify.js';
import {
    addToCart,
    deleteCart,
    GetAllCarts,
    getCart,
    updateCart,
} from '../controllers/cart.js';

import express from 'express';
const router = express.Router();

// add to cart/update cart
router.post('/add/:id', verifyTokenAndAuth, addToCart);

// Update Cart
router.put('/:id/update', verifyTokenAndAuth, updateCart);

// Delete Cart
router.delete('/:id/delete', verifyTokenAndAuth, deleteCart);

// Get User Cart
router.get('/:id', verifyTokenAndAuth, getCart);

// Get All Carts
router.get('/', verifyTokenAndAdmin, GetAllCarts);

export default router;
