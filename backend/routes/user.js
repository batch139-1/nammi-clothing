import { verifyTokenAndAuth, verifyTokenAndAdmin } from './verify.js';
import express from 'express';
import {
    deleteUser,
    getAllUsers,
    getUser,
    setAsAdmin,
    updateUser,
} from '../controllers/user.js';

const router = express.Router();

// Update isAdmin (Admin only)
router.put('/:id/setAsAdmin', verifyTokenAndAuth, setAsAdmin);

// Update User (Admin Only)
router.put('/:id/update', verifyTokenAndAuth, updateUser);

// Delete User (Auth only)
router.delete('/:id/delete', verifyTokenAndAuth, deleteUser);

// Get User(Auth only)
router.get('/:id', verifyTokenAndAuth, getUser);

// Get All Users (Admin only)
router.get('/', verifyTokenAndAdmin, getAllUsers);

export default router;
