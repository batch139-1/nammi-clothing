import {
    verifyToken,
    verifyTokenAndAuth,
    verifyTokenAndAdmin,
} from './verify.js';

import express from 'express';
import {
    checkOut,
    deleteOrder,
    getAllOrders,
    getOrder,
    updateOrder,
} from '../controllers/order.js';
const router = express.Router();

// Checkout Order
router.post('/:id/checkout', verifyToken, checkOut);

// Update Order
router.put('/:id/update', verifyToken, updateOrder);

// Delete Order
router.delete('/:id/delete', verifyToken, deleteOrder);

// Get User Order
router.get('/:id', verifyTokenAndAuth, getOrder);

// Get All Orders
router.get('/', verifyTokenAndAdmin, getAllOrders);

export default router;
