import { verifyTokenAndAdmin } from './verify.js';
import Product from '../models/Product.js';

import express from 'express';
import {
    createProduct,
    deleteProduct,
    getAllProducts,
    getProduct,
    getStockedProducts,
    updateProduct,
} from '../controllers/product.js';
const router = express.Router();

// Create Product (Admin only)
router.post('/create', verifyTokenAndAdmin, createProduct);

// Update Product (Admin only)
router.put('/:id/update', verifyTokenAndAdmin, updateProduct);

// Delete Product (Admin Only)
router.delete('/:id/delete', verifyTokenAndAdmin, deleteProduct);

// Get Product (For all)
router.get('/find/:id', getProduct);

// Get all stocked products
router.get('/stocked', getStockedProducts);

// Get All Products
router.get('/', getAllProducts);

export default router;
