import express from 'express';
const router = express.Router();

import { loginUser, registerUser } from '../controllers/auth.js';

// User Registration
router.post('/register', registerUser);

// User Login
router.post('/login', loginUser);

export default router;
